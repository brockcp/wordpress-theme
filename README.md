# Wordpress Theme

- A fully customizable **Wordpress Theme** built on **Underscore's Automattic Starter Template**.

### Features:

  - **Did I mention it was fully customizable?**
- Using Wordpress' built in **Customizer** all text can easily be modified as well as all colors without any coding.
- Header background image and logo graphic can also be modified via the Customizer.
- Fully responsive - scales up and down for use on all computers and mobile devices.

### Technologies used:

- Built on **Underscore's Wordpress Starter Template**.
- **AOS**(Animate On Scroll) for css animations.
- **Fontawesome** for icons.
- **jQuery** for javascript functionality.
- **Bootstrap** for css standardization.

### This theme can be viewed live [here](http://ocwebworks.com/works/wordpress/).

### Setup Instructions:

-coming soon...
