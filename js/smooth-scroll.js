jQuery(function( $ ){

  //WORKS LIKE BELOW https://stackoverflow.com/questions/27352232/smooth-scroll-to-name
	$('a[href^="#"]').click(function(e) {
        e.preventDefault();
		var target = $(this.hash);
        if (target.length == 0){target = $('a[name="' + this.hash.substr(1) + '"]');}
        if (target.length == 0){target = $('html');}
        console.log(target);
		$('html, body').animate({ scrollTop: target.offset().top }, 1500);
		return false;
	});

  //WORKS BUT A LOT OF CODE (CSS TRICKS)
  // $('a[href*="#"]')
  //   // Remove links that don't actually link to anything
  //   .not('[href="#"]')
  //   .not('[href="#0"]')
  //   .click(function(event) {
  //     // On-page links
  //     if (
  //       location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
  //       &&
  //       location.hostname == this.hostname
  //     ) {
  //       // Figure out element to scroll to
  //       var target = $(this.hash);
  //       target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
  //       // Does a scroll target exist?
  //       if (target.length) {
  //         // Only prevent default if animation is actually gonna happen
  //         event.preventDefault();
  //         $('html, body').animate({
  //           scrollTop: target.offset().top
  //         }, 1000, function() {
  //           // Callback after animation
  //           // Must change focus!
  //           var $target = $(target);
  //           $target.focus();
  //           if ($target.is(":focus")) { // Checking if the target was focused
  //             return false;
  //           } else {
  //             $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
  //             $target.focus(); // Set focus again
  //           };
  //         });
  //       }
  //     }
  //   });

});
