<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package brocks_theme_2
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
		  <div class="footer">
				<i class="far fa-copyright"></i>
        <span class="year">2020</span>
				<span class="company"><?php bloginfo( 'name' ); ?></span>
			  <span class="rights"> All Rights Reserved </span>
		</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
