<?php
add_action( 'customize_register', 'brocks_theme_2_customize_register' );
function brocks_theme_2_customize_register( $wp_customize ) {

	require_once( dirname( __FILE__ ) . '../../alpha-color-picker.php' );

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_section( 'colors' ); //bp added
	$wp_customize->remove_section( 'header_image' ); //bp added
	$wp_customize->remove_section( 'background_image' ); //bp added

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'brocks_theme_2_customize_partial_blogname',
		));
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'brocks_theme_2_customize_partial_blogdescription',
		));
	}
	//PANEL1
	$wp_customize->add_panel( 'Panel1', array(
		 'title' => 'BP Style',
		 'description' => "Brock Patterson Theme",
		 'priority' => 10,
	));

	    //SECTION - BACKGROUND IMAGE
		  $wp_customize->add_section( 'Background_Image_Section' , array(
			    'title'       => 'Background Image',
			    'description' => 'Choose an image for your header background.',
			    'priority'    => 10,
			    'panel'       => 'Panel1',
		  ));
			  	$wp_customize->add_setting( 'Background_Image_Setting' );
				  $wp_customize->add_control(
						new WP_Customize_Image_Control( $wp_customize, 'Background_Image_Setting', array(
					  'label'    => __( 'Current Background Image'),
					  'section'  => 'Background_Image_Section',
					  'settings' => 'Background_Image_Setting',
					)));

	    //SECTION - BRANDING COLORS
			$wp_customize->add_section( 'Branding_Colors_Section' , array(
				'title'        => "Branding Colors",
				'description'  => "Choose your Logo and Slogan Font Colors",
				'priority'     => 20,
				'panel'        => 'Panel1',
			));
			    //BRANDING LOGO COLOR
					$wp_customize->add_setting( 'Branding_Logo_Color_Setting' , array(
						//'default'    => '#ffffff',
						'transport'  => 'refresh',
					));
							$wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, 'Branding_Logo_Color_Setting', array(
							  'label'      => __( 'Logo Font Color' ),
							  'section'    => 'Branding_Colors_Section',
							  'settings'   => 'Branding_Logo_Color_Setting',
							)));
					//BRANDING SLOGAN COLOR
					$wp_customize->add_setting( 'Branding_Slogan_Color_Setting' , array(
						//'default'    => '#ffffff',
						'transport'  => 'refresh',
					));
							$wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, 'Branding_Slogan_Color_Setting', array(
							  'label'      => __( 'Slogan Font Color' ),
							  'section'    => 'Branding_Colors_Section',
							  'settings'   => 'Branding_Slogan_Color_Setting',
							)));

			//SECTION - MENU COLORS
 			$wp_customize->add_section( 'Menu_Colors_Section' , array(
 				'title'      => 'Menu Colors',
			  'description' => 'Choose the font and background colors for your menu.',
 				'priority'   => 30,
 				'panel'      => 'Panel1',
 			));
 			    //MENU FONT COLOR
 					$wp_customize->add_setting( 'Menu_Font_Color_Setting' , array(
 						//'default'    => '#ffffff',
 						'transport'  => 'refresh',
 					));
		 					$wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, 'Menu_Font_Color_Setting', array(
		 					  'label'      => __( 'Menu Font Color' ),
		 					  'section'    => 'Menu_Colors_Section',
		 					  'settings'   => 'Menu_Font_Color_Setting',
		 					)));
					//MENU BACKGROUND COLOR
					$wp_customize->add_setting('Menu_Bg_Color_Setting', array(
						//'default'     => 'rgba(100,100,100,0.3)',
						'type'        => 'theme_mod',
						'capability'  => 'edit_theme_options',
						'transport'   => 'refresh'
						)
					);
							$wp_customize->add_control(
								new Customize_Alpha_Color_Control( $wp_customize, 'Menu_Bg_Color_Setting', array(
										'label'         => __( 'Menu Alpha Background Color'),
										'section'       => 'Menu_Colors_Section',
										'settings'      => 'Menu_Bg_Color_Setting',
										'show_opacity'  => true,
										'palette'	=> array(
											'rgb(50, 50, 50)',
											'rgba(50,50,50,0.8)',
											'rgba( 255, 255, 255, 0.2 )',
											'#ffffff'
										)
									)
								)
							);

			//SECTION - CALLOUT MESSAGE
			$wp_customize->add_section( 'Callout_Message_Section' , array(
				'title'      => 'Callout Message',
				'description'=> 'The callout is the slideup Hero message',
				'priority'   =>  40,
				'panel'      => 'Panel1',
			));
			    //CALLOUT MESSAGE - TEXT
					$wp_customize->add_setting('Callout_Message_Text_Setting', array(
						'default'    =>  "Your callout here!",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control(
								new WP_Customize_Control( $wp_customize, 'Callout_Message_Text_Setting', array(
								"label"      =>  __( "Add your callout message." ),
								"section"    =>  'Callout_Message_Section',
								"type"       =>  'text',
							)));
					//CALLOUT MESSAGE - FONT COLOR
					$wp_customize->add_setting("Callout_Message_Font_Color_Setting", array(
						//"default"    =>  "#ff3300",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, "Callout_Message_Font_Color_Setting", array(
								"label"      => __( "Callout Font Color" ),
								"section"    => "Callout_Message_Section",
								"settings"   => "Callout_Message_Font_Color_Setting",
							)));
					//CALLOUT MESSAGE - BACKGROUND COLOR
					$wp_customize->add_setting('Callout_Message_Bg_Color_Setting', array(
							//'default'     => 'rgba(100,100,100,0.3)',
							'type'        => 'theme_mod',
							'capability'  => 'edit_theme_options',
							'transport'   => 'refresh'
						)
					);
							$wp_customize->add_control(
								new Customize_Alpha_Color_Control( $wp_customize, 'Callout_Message_Bg_Color_Setting', array(
										'label'         => __( 'Callout Alpha Background Color'),
										'section'       => 'Callout_Message_Section',
										'settings'      => 'Callout_Message_Bg_Color_Setting',
										'show_opacity'  => true,
										'palette'	=> array(
											'rgb(150, 50, 220)',
											'rgba(50,50,50,0.8)',
											'rgba( 255, 255, 255, 0.2 )',
											'#00CC99'
										)
									)
								)
							);

      //SECTION - INTRODUCTION
			$wp_customize->add_section( 'Intro_Section' , array(
				'title'      => 'Introduction Section',
				'priority'   => 50,
				'panel'      => 'Panel1',
			));
			    //INTRO TITLE TEXT
					$wp_customize->add_setting('Intro_Title_Text_Setting', array(
						'default'    =>  "Intro Title",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control(
								new WP_Customize_Control( $wp_customize, 'Intro_Title_Text_Setting', array(
								"label"      =>  __( "Add your introduction title here." ),
								"section"    =>  'Intro_Section',
								"type"       =>  'text',
							)));
					//INTRO TITLE FONT COLOR
					$wp_customize->add_setting("Intro_Title_Font_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, "Intro_Title_Font_Color_Setting", array(
								"label"      => __( "Introduction Title Font Color" ),
								"section"    => "Intro_Section",
								"settings"   => "Intro_Title_Font_Color_Setting",
							)));
					//INTRO BODY TEXT
					$wp_customize->add_setting('Intro_Body_Text_Setting', array(
						'default'    =>  "Intro Body",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control(
								new WP_Customize_Control( $wp_customize, 'Intro_Body_Text_Setting', array(
								"label"      =>  __( "Add your introduction paragraph here." ),
								"section"    =>  'Intro_Section',
								"type"       =>  'text',
							)));
          //INTRO BODY FONT COLOR
					$wp_customize->add_setting("Intro_Body_Font_Color_Setting", array(
						//"default"   =>  "#555555",
						"transport" =>  "refresh",
					));
							$wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, "Intro_Body_Font_Color_Setting", array(
								"label"     =>  __( "Introduction Paragraph Font Color" ),
								"section"   =>  "Intro_Section",
								"settings"  =>  "Intro_Body_Font_Color_Setting",
							)));
          //INTRO BODY BG COLOR
					$wp_customize->add_setting("Intro_Body_Bg_Color_Setting", array(
						//"default"    =>  "#333333",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "Intro_Body_Bg_Color_Setting", array(
								"label"      => __( "Introduction Section Background Color" ),
								"section"    => "Intro_Section",
								"settings"   => "Intro_Body_Bg_Color_Setting",
							)));

			//SECTION - ABOUT
			$wp_customize->add_section( 'About_Section' , array(
				'title'      => 'About Section',
				'priority'   => 60,
				'panel'      => 'Panel1',
			));
					//ABOUT 1 TITLE
					$wp_customize->add_setting('About1_Title_Setting', array(
						'default'    =>  "About 1",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'About1_Title_Setting', array(
								"label"      =>  __( "Text For About Title 1" ),
								"section"    =>  'About_Section',
								"type"       =>  'text',
							)));
					//ABOUT 1 TITLE FONT COLOR
					$wp_customize->add_setting("About1_Title_Font_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "About1_Title_Font_Color_Setting", array(
								"label"      => __( "Font Color for About Title 1" ),
								"section"    => "About_Section",
								"settings"   => "About1_Title_Font_Color_Setting",
							)));
					//ABOUT 1 BODY
					$wp_customize->add_setting('About1_Body_Setting', array(
						'default'    =>  "About Body 1",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'About1_Body_Setting', array(
								"label"      =>  __( "Text For About Body 1" ),
								"section"    =>  'About_Section',
								"type"       =>  'text',
							)));
					//ABOUT 1 BODY FONT COLOR
					$wp_customize->add_setting("About1_Body_Font_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "About1_Body_Font_Color_Setting", array(
								"label"      => __( "Font Color for About Body 1" ),
								"section"    => "About_Section",
								"settings"   => "About1_Body_Font_Color_Setting",
							)));
					//ABOUT 2 TITLE
					$wp_customize->add_setting('About2_Title_Setting', array(
						'default'    =>  "About 2",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'About2_Title_Setting', array(
								"label"      =>  __( "Text For About Title 2" ),
								"section"    =>  'About_Section',
								"type"       =>  'text',
							)));
					//ABOUT 2 TITLE FONT COLOR
					$wp_customize->add_setting("About2_Title_Font_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "About2_Title_Font_Color_Setting", array(
								"label"      => __( "Font Color for About Title 2" ),
								"section"    => "About_Section",
								"settings"   => "About2_Title_Font_Color_Setting",
							)));
					//ABOUT 2 BODY
					$wp_customize->add_setting('About2_Body_Setting', array(
						'default'    =>  "About Body 2",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'About2_Body_Setting', array(
								"label"      =>  __( "Text For About Body 2" ),
								"section"    =>  'About_Section',
								"type"       =>  'text',
							)));
					//ABOUT 2 BODY FONT COLOR
					$wp_customize->add_setting("About2_Body_Font_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "About2_Body_Font_Color_Setting", array(
								"label"      => __( "Font Color for About Body 2" ),
								"section"    => "About_Section",
								"settings"   => "About2_Body_Font_Color_Setting",
							)));
					//ABOUT 3 TITLE
					$wp_customize->add_setting('About3_Title_Setting', array(
						'default'    =>  "About 3",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'About3_Title_Setting', array(
								"label"      =>  __( "Text For About Title 3" ),
								"section"    =>  'About_Section',
								"type"       =>  'text',
							)));
					//ABOUT 3 TITLE FONT COLOR
					$wp_customize->add_setting("About3_Title_Font_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "About3_Title_Font_Color_Setting", array(
								"label"      => __( "Font Color for About Title 3" ),
								"section"    => "About_Section",
								"settings"   => "About3_Title_Font_Color_Setting",
							)));
					//ABOUT 3 BODY
					$wp_customize->add_setting('About3_Body_Setting', array(
						'default'    =>  "About Body 3",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'About3_Body_Setting', array(
								"label"      =>  __( "Text For About Body 3" ),
								"section"    =>  'About_Section',
								"type"       =>  'text',
							)));
					//ABOUT 3 BODY FONT COLOR
					$wp_customize->add_setting("About3_Body_Font_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "About3_Body_Font_Color_Setting", array(
								"label"      => __( "Font Color for About Body 3" ),
								"section"    => "About_Section",
								"settings"   => "About3_Body_Font_Color_Setting",
							)));
					//ABOUT BACKGROUND COLOR
					$wp_customize->add_setting("About_Background_Color_Setting", array(
						//"default"    =>  "#444444",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "About_Background_Color_Setting", array(
								"label"      => __( "About Section Background Color" ),
								"section"    => "About_Section",
								"settings"   => "About_Background_Color_Setting",
							)));

			//SECTION - CONTACT
			$wp_customize->add_section( 'Contact_Section' , array(
				'title'      => 'Contact Section',
				'priority'   => 70,
				'panel'      => 'Panel1',
			));
					//CONTACT 1 TEXT
					$wp_customize->add_setting('Contact_Setting1', array(
						'default'    =>  "Contact 1",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'Contact_Setting1', array(
								"label"      =>  __( "Enter your email address here" ),
								"section"    =>  'Contact_Section',
								"type"       =>  'text',
							)));
					//CONTACT 1 FONT COLOR
					$wp_customize->add_setting("Contact1_Font_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "Contact1_Font_Color_Setting", array(
								"label"      => __( "Email font color" ),
								"section"    => "Contact_Section",
								"settings"   => "Contact1_Font_Color_Setting",
							)));
							//CONTACT 2 TEXT
							$wp_customize->add_setting('Contact_Setting2', array(
								'default'    =>  "Contact 2",
								'transport'  =>  "postMessage",
							));
									$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'Contact_Setting2', array(
										"label"      =>  __( "Enter your phone number here" ),
										"section"    =>  'Contact_Section',
										"type"       =>  'text',
									)));
					//CONTACT 2 FONT COLOR
					$wp_customize->add_setting("Contact2_Font_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "Contact2_Font_Color_Setting", array(
								"label"      => __( "Phone font color" ),
								"section"    => "Contact_Section",
								"settings"   => "Contact2_Font_Color_Setting",
							)));
					//CONTACT 3 TEXT
					$wp_customize->add_setting('Contact_Setting3', array(
						'default'    =>  "Contact 3",
						'transport'  =>  "postMessage",
					));
							$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'Contact_Setting3', array(
								"label"      =>  __( "Paste your google map url here" ),
								"section"    =>  'Contact_Section',
								"type"       =>  'text',
							)));
					//CONTACT 3 FONT COLOR
					$wp_customize->add_setting("Contact3_Font_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, "Contact3_Font_Color_Setting", array(
								"label"      => __( "Location Font Color" ),
								"section"    => "Contact_Section",
								"settings"   => "Contact3_Font_Color_Setting",
							)));
					//CONTACT ICON COLOR
					$wp_customize->add_setting("Contact_Icon_Color_Setting", array(
						//"default"    =>  "#ffffff",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, "Contact_Icon_Color_Setting", array(
								"label"      => __( "Contact Icon Color" ),
								"section"    => "Contact_Section",
								"settings"   => "Contact_Icon_Color_Setting",
							)));
					//CONTACT BG COLOR
					$wp_customize->add_setting("Contact_Background_Color_Setting", array(
						//"default"    =>  "#555555",
						"transport"  =>  "refresh",
					));
							$wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, "Contact_Background_Color_Setting", array(
								"label"      => __( "Contact Section Background Color" ),
								"section"    => "Contact_Section",
								"settings"   => "Contact_Background_Color_Setting",
							)));

			//SECTION - WIDGETS
			$wp_customize->add_section( 'Widget_Section' , array(
				'title'      => 'Widgets Section',
				'description'=> 'Choose your widget colors',
				'priority'   =>  80,
				'panel'      => 'Panel1',
			));
			    //WIDGETS - TITLE COLOR
			    $wp_customize->add_setting("Widget_Title_Color_Setting", array(
		        //"default"    =>  "#ffffff",
		        "transport"  =>  "refresh",
	        ));
	            $wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, "Widget_Title_Color_Setting", array(
		            "label"      => __( "Font Color for Widget Titles" ),
		            "section"    => "Widget_Section",
	              "settings"   => "Widget_Title_Color_Setting",
	        )));
					//WIDGETS - POST ANCHOR COLOR
			    $wp_customize->add_setting("Widget_Post_Color_Setting", array(
		        //"default"    =>  "#ffffff",
		        "transport"  =>  "refresh",
	        ));
	            $wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, "Widget_Post_Color_Setting", array(
		            "label"      => __( "Font Color for Post Links" ),
		            "section"    => "Widget_Section",
	              "settings"   => "Widget_Post_Color_Setting",
	        )));
			    //WIDGETS - BACKGROUND COLOR
          $wp_customize->add_setting("Widget_Background_Color_Setting", array(
						//"default"    =>  "#333333",
						"transport"  =>  "refresh",
					));
						  $wp_customize->add_control(
								new WP_Customize_Color_Control( $wp_customize, "Widget_Background_Color_Setting", array(
								"label"      => __( "Widget Section Background Color" ),
								"section"    => "Widget_Section",
								"settings"   => "Widget_Background_Color_Setting",
							)));

}

add_action( 'wp_head', 'load_settings');
function load_settings(){
  ?>
	  <style type="text/css">
			.site-title a,
			.site-title-header2 a { color: <?php echo get_theme_mod('Branding_Logo_Color_Setting', '#ffffff'); ?>; }
      .site-description,
			.site-description-header2 {color: <?php echo get_theme_mod('Branding_Slogan_Color_Setting', '#ffffff'); ?>; }
			ul li.menu-item a {color: <?php echo get_theme_mod('Menu_Font_Color_Setting', '#ffffff'); ?>; }
			ul.menu {background-color: <?php echo get_theme_mod('Menu_Bg_Color_Setting', 'rgba(100, 100, 100, .3)'); ?>; }
      h1.hero-callout {color: <?php echo get_theme_mod('Callout_Message_Font_Color_Setting'); ?>; }
      h1.hero-callout {background-color: <?php echo get_theme_mod('Callout_Message_Bg_Color_Setting','rgba(100, 100, 100, .3)'); ?>; }

			.intro h1 {color: <?php echo get_theme_mod("Intro_Title_Font_Color_Setting", "#ffffff"); ?>; }
			.intro h3 {color: <?php echo get_theme_mod("Intro_Body_Font_Color_Setting", "#ffffff"); ?>; }
			.intro {background-color: <?php echo get_theme_mod("Intro_Body_Bg_Color_Setting", "#333333"); ?>; }

      .about-sub:first-child h1 {color: <?php echo get_theme_mod("About1_Title_Font_Color_Setting", "#000000"); ?>; }
			.about-sub:first-child p {color: <?php echo get_theme_mod("About1_Body_Font_Color_Setting", "#000000"); ?>; }
			.about-sub:nth-child(2) h1 {color: <?php echo get_theme_mod("About2_Title_Font_Color_Setting", "#000000"); ?>; }
			.about-sub:nth-child(2) p {color: <?php echo get_theme_mod("About2_Body_Font_Color_Setting", "#000000"); ?>; }
			.about-sub:nth-child(3) h1 {color: <?php echo get_theme_mod("About3_Title_Font_Color_Setting", "#000000"); ?>; }
			.about-sub:nth-child(3) p {color: <?php echo get_theme_mod("About3_Body_Font_Color_Setting", "#000000"); ?>; }
			.about { background-color: <?php echo get_theme_mod("About_Background_Color_Setting", "#444444"); ?>; }

			.contact-sub:first-child h3 {color: <?php echo get_theme_mod("Contact1_Font_Color_Setting", "#ffffff"); ?>; }
			.contact-sub:nth-child(2) h3 {color: <?php echo get_theme_mod("Contact2_Font_Color_Setting", "#ffffff"); ?>; }
			.contact-sub:nth-child(3) h3 {color: <?php echo get_theme_mod("Contact3_Font_Color_Setting", "#ffffff"); ?>; }
			.contact { background-color: <?php echo get_theme_mod("Contact_Background_Color_Setting", "#444444"); ?>; }
      .fa-envelope, .fa-phone, .fa-map-marker-alt{ color: <? echo get_theme_mod("Contact_Icon_Color_Setting","#ffffff"); ?>; }
      .widget { background-color: <?php echo get_theme_mod("Widget_Background_Color_Setting", "#333333"); ?>; }
			.widget-title { color: <?php echo get_theme_mod("Widget_Title_Color_Setting", "#ffffff"); ?>; }
			.widget ul li a {color: <?php echo get_theme_mod("Widget_Post_Color_Setting", "#ffffff"); ?>; }
	  </style>
  <?php
}

function brocks_theme_2_customize_partial_blogname() {
	bloginfo( 'name' );
}
function brocks_theme_2_customize_partial_blogdescription() {
	bloginfo( 'description' );
}
function brocks_theme_2_customize_preview_js() {
	wp_enqueue_script( 'brocks_theme_2-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'brocks_theme_2_customize_preview_js' );
