<?php
get_header();
?>
<div class="container-fluid">
  <div class="m-lr--15">

    <div class="header-container">
     <img class="header-img" src="<?php echo esc_url(get_theme_mod( 'Background_Image_Setting')); ?>">
     <h1 class='hero-callout'><?php echo get_theme_mod('Callout_Message_Text_Setting','Your Callout Message Here!')?></h1>
    </div>

    <div class="intro">
        <h1><?php echo get_theme_mod('Intro_Title_Text_Setting','Your Intro Title Here!')?></h1>
        <h3><?php echo get_theme_mod('Intro_Body_Text_Setting','Your Intro Body Here!')?></h3>
    </div>

    <div class="about" id="about">
      <div class="about-sub" data-aos="fadeinup" data-aos-delay="100">
        <h1><?php echo get_theme_mod('About1_Title_Setting','About 1 Title Here.')?></h1>
        <h3><?php echo get_theme_mod("About1_Body_Setting", 'About 1 Body here.')?></h3>
      </div>
      <div class="about-sub" data-aos="fadeinup" data-aos-delay="200">
        <h1><?php echo get_theme_mod('About2_Title_Setting','About 2 Title Here.')?></h1>
        <h3><?php echo get_theme_mod("About2_Body_Setting", 'About 2 Body here.')?></h3>
      </div>
      <div class="about-sub" data-aos="fadeinup" data-aos-delay="300">
        <h1><?php echo get_theme_mod('About3_Title_Setting','About 3 Title Here.')?></h1>
        <h3><?php echo get_theme_mod("About3_Body_Setting", 'About 3 Body here.')?></h3>
      </div>
    </div>

    <div class="contact" id="contact">
      <a class="contact-sub" href="mailto:<?php echo get_theme_mod('Contact_Setting1',"your@email.com")?>" target="_blank"><i class="far fa-envelope"></i><h3>email</h3></a>
      <a class="contact-sub" href="tel:<?php echo get_theme_mod('Contact_Setting2','1-888-888-8888')?>"><i class="fas fa-phone"></i><h3>phone</h3></a>
      <a class="contact-sub" href="<?php echo get_theme_mod('Contact_Setting3','https://www.google.com/maps/@33.4920146,-117.6780621,17z')?>" target="_blank"><i class="fas fa-map-marker-alt"></i><h3>location</h3></a>
    </div>

  </div>
  <script>
    AOS.init({
      easing: 'ease-out',
      duration: '600'
    });
  </script>
</div>
<?php
get_sidebar();
get_footer();
