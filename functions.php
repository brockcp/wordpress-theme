<?php
if ( ! function_exists( 'brocks_theme_2_setup' ) ) :
	function brocks_theme_2_setup() {
		load_theme_textdomain( 'brocks_theme_2', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'brocks_theme_2' ),
		) );
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		add_theme_support( 'custom-background', apply_filters( 'brocks_theme_2_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'brocks_theme_2_setup' );

function brocks_theme_2_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'brocks_theme_2_content_width', 640 );
}
add_action( 'after_setup_theme', 'brocks_theme_2_content_width', 0 );

function brocks_theme_2_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'brocks_theme_2' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'brocks_theme_2' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'brocks_theme_2_widgets_init' );


function brocks_theme_2_scripts() {
	wp_enqueue_style( 'brocks_theme_2-style', get_stylesheet_uri() );
	wp_enqueue_script( 'alpha_js', get_template_directory_uri() . '/alpha-color-picker.js' );
  wp_enqueue_style( 'alpha-color-picker', get_stylesheet_uri() );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri(). '/js/dist/bootstrap-build.js', array('jquery'));
	wp_enqueue_script( 'smooth-scroll', get_template_directory_uri(). '/js/smooth-scroll.js');
	wp_enqueue_script( 'bg-video', get_template_directory_uri(). '/js/bg-video.js');
	wp_enqueue_script( 'AOS', get_template_directory_uri(). '/js/aos.js');
	wp_enqueue_script( 'brocks_theme_2-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'brocks_theme_2-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'brocks_theme_2_scripts' );


#REMOVES WEBSITE INPUT FROM COMMENT FORM -bp
function remove_input($arg) {
    $arg['url'] = '';
    return $arg;
}
add_filter('comment_form_default_fields', 'remove_input');


#CHANGE FORM TAG FIELDS -bp
function change_form_tags( $args ) {
	$args['title_reply'] = __( 'Care to Comment?' );
	$args['label_submit'] = __( 'Submit', 'custom' );
	return $args;
}
add_filter( 'comment_form_defaults', 'change_form_tags' );


require get_template_directory() . '/inc/custom-header.php';
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/template-functions.php';
require get_template_directory() . '/inc/customizer.php';

if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
