<?php
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<!-- <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'brocks_theme_2' ); ?></a> -->
			<?php
			if ( is_front_page() ) :
			?>
				<div class="site-branding-front-page">
				<?php the_custom_logo(); ?>
				<h1 class="site-title-front-page"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1><?php
			else :
			?>
			  <div class="site-branding-general">
				<?php the_custom_logo(); ?>
				<p class="site-title-general"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p><?php
			endif;
			$brocks_theme_2_description = get_bloginfo( 'description', 'display' );
			if ( $brocks_theme_2_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $brocks_theme_2_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->
		<nav>
			<?php
			if ( is_front_page() ) :
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			else :
				?><span class="menu-b-placeholder"></span>
			<?php
		endif;
		 ?>
		</nav><!-- #site-navigation -->
	<div id="content" class="site-content">
