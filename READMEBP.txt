-ACCESS-
 -localhost/wordpress
 -$ npm run css && npm run watch //css runs node-sass and watch runs nodemon (see package.json)
 -$ npm run brows  //runs browsersync which opens localhost:3000/wordpress

 -NOTES-

-FEATURES-
 -WP Customizer for client customization -> see inc/customizer.php amd js/customizer.js
 -AOS -> initialized in front-page.php
 -SCSS -> node-sass compiles css/scss/style.css into style.css
 -smoothscrolling -> see js/smooth-scroll.js

-ISSUES-
 -search results page -> head content
 -nothing found page -> head content
